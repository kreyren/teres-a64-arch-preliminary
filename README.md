DEVELOPMENT HALTED: https://qoto.org/web/statuses/109961967529276490

---

# Preliminary ArchLinux image for Teres-A64

Preliminary image of a system i ducttaped together using u-boot, bootloader, kernel and kernel modules from armbian's https://github.com/armbian/build/pull/4807 and using archlinuxarm generic aarch64 image.

Parabola port is in the works https://labs.parabola.nu/issues/3450

Download from releases: https://git.dotya.ml/kreyren/teres-a64-arch-preliminary/releases

Installation:

    # dd if=./path/to/file.img of=/dev/DEVICE conv=sync status=progress

where `/dev/DEVICE` is your sdcard
